<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.time.*" import="java.time.format.DateTimeFormatter" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>JSP Activity</title>
	</head>
	<body>
		
		<!-- Using the scriptlet tag, create a variable dateTime-->
		<%!
			LocalDateTime dateTime = LocalDateTime.now();
			String pattern = "yyyy-MM-dd HH:mm:ss";
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
			String timeFormat = dateTime.format(formatter);
		%>
	<!-- Use the LocalDateTime and DateTimeFormatter classes -->
	<!-- Change the pattern to "yyyy-MM-dd HH:mm:ss" -->
  	
  	<!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->
  	<h1>Our Date and Time now is...</h1>
	<ul>
		<li> Manila: <%=dateTime.format(formatter) %> </li>
		<li> Japan: <%=dateTime.plusHours(1).format(formatter)%></li>
		<li> Germany: <%=dateTime.minusHours(7).format(formatter)%> </li>
	</ul>
	
	<!-- Given the following Java Syntax below, apply the correct JSP syntax -->
	<%! 
		int initVar=3;
		int serviceVar=3;
		int destroyVar=3;
	%>
	
	<%! 
	  	public void jspInit(){
		    initVar--;
		    System.out.println("jspInit(): init"+initVar);
		  }
	  	public void jspDestroy(){
	    	destroyVar--;
	    	destroyVar = destroyVar + initVar;
	    	System.out.println("jspDestroy(): destroy"+destroyVar);
	  	}
  	%>
	
	<% 
	  	serviceVar--;
	  	System.out.println("_jspService(): service"+serviceVar);
	  	String content1="content1 : "+initVar;
	  	String content2="content2 : "+serviceVar;
	  	String content3="content3 : "+destroyVar;
  	%>
	
	<h1>JSP</h1>
	<p><%=content1 %></p>
	<p><%=content2 %></p>
	<p><%=content3 %></p>
		
	</body>
	</html>